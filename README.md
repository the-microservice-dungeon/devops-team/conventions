
<!-- TOC -->
* [conventions](#conventions)
  * [Getting started](#getting-started)
* [helm-chart](#helm-chart)
* [Variables](#variables)
* [yaml files](#yaml-files)
  * [Chart.yaml](#chartyaml)
  * [values](#values)
  * [_helpers.tpl](#_helperstpl)
  * [Configmap](#configmap)
  * [Deployment](#deployment)
  * [Ingress](#ingress)
  * [Loadbalancer](#loadbalancer)
  * [Services](#services)
  * [Serviceaccount](#serviceaccount)
  * [fleet](#fleet)
<!-- TOC -->

# conventions

## Getting started

This file shows, how helm files and yaml files should look.

https://helm.sh/docs/

https://helm.sh/docs/chart_template_guide/values_files/

https://helm.sh/docs/topics/charts/

https://helm.sh/docs/chart_template_guide/

https://helm.sh/docs/chart_best_practices/


CURRENTLY EXAMPLE

---

# helm-chart

|             |                     |                           |
|-------------|---------------------|---------------------------|
| charts      |                     |                           |
| templates   |                     |                           |
|             | _helper.tpl         | Templates to use in Chart |
|             | deployment.yaml     | Deployment of Chart       |
|             | service.yaml        |                           |
|             | loadbalancer.yaml   |                           |
|             | serviceaccount.yaml |                           |
|             | ingress.yaml        |                           |
| .helmignore |                     |                           |
| Chart.yaml  |                     |                           |
| valuse.yaml |                     |                           |
|             |                     |                           |
|             |                     |                           |
|             |                     |                           |
|             |                     |                           |




---

# Variables

| Variable |               | Description                                                | Resources                                  |
|----------|---------------|------------------------------------------------------------|--------------------------------------------|
| service  | -             | section of pod service (communication internal)            | service, loadbalancer, ingress, deployment |      
| ---      | port          | port inside of kubernetes cluster                          | service, loadbalancer, ingress             |
| ---      | targetPort    | port of application                                        | service                                    |
| ---      | nodePort      | port on node                                               | loadbalancer                               |         
| ---      | containerPort | container listen on this port <br> (value is service.port) | deployment                                 |
| ingress  | -             | ingress ressource (communication over url)                 | ingress                                    |
| ---      | .port.number  | internal port-number of service (service.port)             | ingress                                    |




# yaml files

conventions for our helm charts

## Chart.yaml
```yaml
apiVersion: v2
name: changeme
description: Helm Chart for the core service changeme

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 1.1.2

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
# It is recommended to use it with quotes.
appVersion: "latest"

dependencies:
  - name: mysql
    version: 10.2.2
    repository: https://charts.bitnami.com/bitnami
```

---

## values
```yaml


nameOverride: ""
fullnameOverride: ""
replicas: 1

image:
  registry: "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry"
  name: "changeme"
  # set for specific version, if not set, value is Chart appVersion
  tag: latest
  pullPolicy: Always


serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

service:
  ports:
    - name: http
      port: 8080
      targetPort: 8080
      protocol: TCP
      type: ClusterIP

  lb:
    enabled: true
    ports:
      - port: 8080
        targetPort: 8080
        lbPort: 30002
        protocol: TCP
        type: LoadBalancer

ingress:
  enabled: false
  hosts:
    - hostname: changeme.msd.dungeon-space.de
      path: /
      classname: nginx
      port: 8080
      pathType: Prefix



readinessProbe:
  httpGet:
    path: /actuator/health
    port: http
  initialDelaySeconds: 10
  periodSeconds: 10
  failureThreshold: 3
  successThreshold: 1

livenessProbe:
  httpGet:
    path: /actuator/health
    port: http
  initialDelaySeconds: 40
  periodSeconds: 10
  failureThreshold: 3
  successThreshold: 1

resources:
  requests:
    memory: "128Mi"
    cpu: "100m"
  limits:
    memory: "2560Mi"
    cpu: "2000m"

configmap:
  data:
    KAFKA_BOOTSTRAP_ADDRESS: "msd-redpanda.redpanda-system.svc.cluster.local:9093"
    RABBITMQ_HOST: "rabbitmq-service.rabbitmq.svc.cluster.local"
    RABBITMQ_PORT: "5672"
    RABBITMQ_USER: "admin"
    RABBITMQ_PASSWORD: "admin"
    ROBOT_SERVICE: "robot.robot.svc.cluster.local:8080"
    MAP_SERVICE: "map.map.svc.cluster.local:8080"
    GAMELOG_SERVICE: "gamelog.gamelog.svc.cluster.local:8080"
    TRADING_SERVICE: "trading.trading.svc.cluster.local:8080"
    GAME_SERVICE: "game.game.svc.cluster.local:8080"

env:
  - name: DB_NAME
    value: changemedb
  - name: DB_HOST
    value: changeme-mysql.changeme.svc.cluster.local:3306
  - name: DB_USER
    value: changeme
  - name: DB_PASSWORD
    value: changeme

mysql:
  global:
    storageClass: standard
  auth:
    rootPassword: rooter
    database: changemedb
    username: changeme
    password: changeme
```

---

## _helpers.tpl
```yaml
{{/*
Expand the name of the chart.
*/}}
{{- define "changeme.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "changeme.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "changeme.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "changeme.labels" -}}
helm.sh/chart: {{ include "changeme.chart" . }}
{{ include "changeme.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "changeme.selectorLabels" -}}
app.kubernetes.io/name: {{ include "changeme.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "changeme.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "changeme.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

```

---

## Configmap
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name:  {{ include "changeme.fullname" . }}
data:
  {{- tpl (toYaml .Values.configmap.data) . | nindent 2 }}
```

---

## Deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name:  {{ include "changeme.fullname" . }}
  labels:
    {{- include "changeme.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicas | int }} 
  selector:
    matchLabels:
      {{- include "changeme.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "changeme.selectorLabels" . | nindent 8 }}
    spec:
      serviceAccountName: {{ include "changeme.serviceAccountName" . }}
      containers:
      - name: {{ .Chart.Name }}
        image: "{{ .Values.image.registry }}/{{ .Values.image.name }}:{{ .Values.image.tag | default .Chart.Version }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        ports:
        {{- range .Values.service.ports }}
          - name: {{ .name | lower }}
            containerPort: {{ .port }}
            protocol: {{ .protocol }}
            {{- end }}
        {{- if .Values.resources }}
        resources:
          {{- toYaml .Values.resources | nindent 10 }}
        {{- end }}
        {{- if .Values.livenessProbe }} 
        livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 10 }}
        {{- end }}
        {{- if .Values.readinessProbe }} 
        readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 10 }}
        {{- end }}
        envFrom:
          - configMapRef:
              name: {{ include "changeme.fullname" . }}
        {{- if .Values.env }}
        env:
        {{- toYaml .Values.env | nindent 8 }}
        {{- end }}
```

---

## Ingress
```yaml
{{- if eq .Values.ingress.enabled true}}
{{- $fullname := include "changeme.fullname" . -}}
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ include "changeme.fullname" . }}
spec:
  rules:
    {{- range .Values.ingress.hosts }}
    - host: {{ .hostname }}
      http:
        paths:
          - path: {{ .path }}
            pathType: {{ .pathType }}
            backend:
              service:
                name: {{ $fullname }}
                port:
                  number: {{ .port }}
    {{- end }}
{{- end }}
```

---

## Loadbalancer
```yaml
{{ if eq .Values.service.lb.enabled true }}
apiVersion: v1
kind: Service
metadata:
  name: {{ include "changeme.fullname" . }}-lb
  labels:
    {{- include "changeme.labels" . | nindent 4 }}
spec:
{{- range .Values.service.lb.ports }}
  type: {{ .type }}
  ports:
    - port: {{ .port }}
      targetPort: {{ .targetPort }}
      nodePort: {{ .lbPort }}
      protocol: {{ .protocol }}
    {{- end }}
  selector:
    {{- include "changeme.selectorLabels" . | nindent 4 }}
    {{- end }}
```

---

## Services
```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ include "changeme.fullname" . }}
  labels:
          {{- include "changeme.labels" . | nindent 4 }}
spec:
        {{- range .Values.service.ports }}
type: {{ .type }}
ports:
  - port: {{ .port }}
    targetPort: {{ .targetPort }}
    protocol: {{ .protocol }}
    name: {{ .name }}
        {{- end }}
selector:
        {{- include "changeme.selectorLabels" . | nindent 4 }}
```

---

## Serviceaccount
```yaml
{{- if .Values.serviceAccount.create -}}
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "changeme.serviceAccountName" . }}
  labels:
    {{- include "changeme.labels" . | nindent 4 }}
  {{- with .Values.serviceAccount.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
{{- end }}

```

---

## fleet
```yaml
namespace: changeme
helm:
  releaseName: changeme
  targetNamespace: changeme
  repo: "https://gitlab.com/api/v4/projects/42239222/packages/helm/stable"
  chart: "changeme"
  values:
    nameOverride: ""
    fullnameOverride: ""
    replicas: 1

    image:
      registry: "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry"
      name: "changeme"
      # set for specific version, if not set, value is Chart version
      # tag: latest
      pullPolicy: Always


    serviceAccount:
      # Specifies whether a service account should be created
      create: true
      # Annotations to add to the service account
      annotations: {}
      # The name of the service account to use.
      # If not set and create is true, a name is generated using the fullname template
      name: ""

    service:
      ports:
        - name: http
          port: 8080
          targetPort: 8080
          protocol: TCP
          type: ClusterIP

      lb:
        enabled: false

    ingress:
      enabled: true
      hosts:
        - hostname: changeme.msd.dungeon-space.de
          path: /
          classname: nginx
          port: 8080
          pathType: Prefix

    readinessProbe:
      httpGet:
        path: /actuator/health
        port: http
      initialDelaySeconds: 10
      periodSeconds: 10
      failureThreshold: 3
      successThreshold: 1

    livenessProbe:
      httpGet:
        path: /actuator/health
        port: http
      initialDelaySeconds: 40
      periodSeconds: 10
      failureThreshold: 3
      successThreshold: 1

    resources:
      requests:
        memory: "128Mi"
        cpu: "100m"
      limits:
        memory: "2560Mi"
        cpu: "2000m"

    configmap:
      data:
        KAFKA_BOOTSTRAP_ADDRESS: "redpanda-0.redpanda.redpanda-system.svc.cluster.msd.:9093"
        RABBITMQ_HOST: "rabbitmq-service.rabbitmq.svc.cluster.msd"
        RABBITMQ_PORT: "5672"
        RABBITMQ_USER: "admin"
        RABBITMQ_PASSWORD: "admin"
        ROBOT_SERVICE: "robot.robot.svc.cluster.msd:8080"
        MAP_SERVICE: "map.map.svc.cluster.msd:8080"
        GAMELOG_SERVICE: "gamelog.gamelog.svc.cluster.msd:8080"
        TRADING_SERVICE: "trading.trading.svc.cluster.msd:8080"
        GAME_SERVICE: "game.game.svc.cluster.msd:8080"

    env:
      - name: DB_NAME
        value: changemedb
      - name: DB_HOST
        value: changeme-mysql.changeme.svc.cluster.msd:3306
      - name: DB_USER
        value: changeme
      - name: DB_PASSWORD
        value: changeme

    mysql:
      global:
        storageClass: harvester
      auth:
        rootPassword: rooter
        database: changemedb
        username: changeme
        password: changeme
```